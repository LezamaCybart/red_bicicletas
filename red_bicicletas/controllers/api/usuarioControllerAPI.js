var Usuario = require('../../models/usuario');

exports.usuarios_list = (req, res) => {
  Usuario.find({}, (err, usuarios) => {
    res.status(200).json({
      usuarios
    })
  })
}

exports.usuarios_create = function(req, res){
    var ususario = new Usuario({nombre: req.body.nombre});

    ususario.save(function(err){
        res.status(200).json(ususario);
    });
};

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body, function(err, usuario){
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
            console.log('reserva!!!');
            res.status(200).send();
        });
    });
};